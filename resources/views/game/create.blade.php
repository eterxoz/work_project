@extends('layouts.app')

@section('content')
<div class="container">
    <form method="get" action="{{route('game')}}">
        @csrf
        <label>Tytuł</label>
        <input type="text" name="title"><br>
        <label>Opis</label>
        <input type="text" name="body">
        <input type="submit">

    </form>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
</div>

@endsection