@extends('layouts.app')

@section('content')
<div class="container">
    @if(Auth::check() && Auth::user()->is_admin)
    <a class="btn btn-outline-primary" href="{{route('create')}}">Dodaj</a>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <div class="card-body">

                    <div class="col"><h1>Gry możliwe do wypożyczenia</h1></div>
                        @foreach ($games as $game)
                            <p>Tytuł: {{$game->title}}
                            @if(Auth::check() && Auth::user()->is_admin)
                                <a class="btn btn-outline-primary" href="#"> Edytuj</a>
                                <a class="btn btn-outline-primary" href="#"> Usuń</a> 
                            @endif
                                <a class="btn btn-outline-primary" href="#">Wypożycz</a>
                                <a class="btn btn-outline-primary" href="#">Opis</a>
                            </p>

                                
                                

                        @endforeach
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>

                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection