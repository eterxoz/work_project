<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class GameController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
		
		$games = DB::table('games')->get();

    	return view('game.index', compact('games'));
    }

    public function create(Request $req){
		
		$title = $req->input('title');
		$body = $req->input('body');
    	$data = array('title' => $title, 'body' => $body, 'created_at' => now(), 'updated_at' => now());
		if ($body && $title){
			DB::table('games')->insert($data);
		}
    	

    	
    	return view('game.create');
    }
}
